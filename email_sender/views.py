from django.contrib import messages
from django.views.generic.edit import FormView
from django.shortcuts import redirect
from .forms import SendAsynchronousEmails
from .tasks import send_email


# A page with the form where we can input the number of email to send
class GenerateRandomUserView(FormView):
    template_name = 'django_celery/send_emails.html'
    form_class = SendAsynchronousEmails

    def form_valid(self, form):
        total = form.cleaned_data.get('total')
        email_address = form.cleaned_data.get('email_address')
        delay = form.cleaned_data.get('delay')
        for i in range(0, total):
            send_email(email_address)
        messages.success(self.request, 'Emails registered in queue and are being processed.')
        return redirect('send_async_emails')
