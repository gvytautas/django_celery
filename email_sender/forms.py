from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator

class SendAsynchronousEmails(forms.Form):
    total = forms.IntegerField(
        validators=[
            MinValueValidator(5),
            MaxValueValidator(30)
        ]
    )
    email_address = forms.EmailField(
        required=True
    )
    delay = forms.IntegerField(
        validators=[
            MinValueValidator(1),
            MaxValueValidator(30)
        ]
    )