from django.core.mail import send_mail
from celery import shared_task


@shared_task
def send_email(email_address):
    send_mail(
        'Django-Celery Project',
        'Body',
        'from@example.com',
        [email_address],
        fail_silently=False,
    )